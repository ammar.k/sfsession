<?php

namespace App\Controller;

use App\Entity\Professeur;
use App\Form\ProfesseurType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfesseurController extends AbstractController
{
    #[Route('/professeur', name: 'app_professeur')]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $professeurs = $entityManager->getRepository(Professeur::class)->findAll();
        //return new Response('Check out this great product: '.$stagiaire->getName());
        // or render a template
        // in the template, print things with {{ product.name }}
        return $this->render('professeur/index.html.twig', [
            'professeurs' => $professeurs
        ]);
    }

    #[Route('/professeur/new', name: 'new_professeur')]
    #[Route('/professeur/{id}/edit', name: 'edit_professeur')]
    public function new_edit(Professeur $professeur = null, EntityManagerInterface $entitymanager, Request $request) :Response
    {
        if(!$professeur){
            $professeur = new Professeur();
        }
        $form = $this->createForm(ProfesseurType::class, $professeur);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $professeur = $form->getData();
            $entitymanager->persist($professeur);
            $entitymanager->flush();
            return $this->redirectToRoute('app_professeur');
        }
        return $this->render('professeur/new.html.twig', [
            'formAddProfesseur' => $form,
        ]);

    }

    #[Route('/professeur/{id}/delete', name: 'delete_professeur')]
    public function delete(Professeur $professeur, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($professeur);
        $entityManager->flush();
        return $this->redirectToRoute('app_professeur');
    }


    #[Route('/professeur/{id}', name: 'show_professeur')]
    public function infoProfesseur(Professeur $professeur){
        return $this->render('professeur/show.html.twig', [
            'professeur' => $professeur
        ]);
    }
}
