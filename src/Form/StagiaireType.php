<?php

namespace App\Form;

use App\Entity\Stagiaire;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StagiaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('sex', TextType::class)
            ->add('dateNaissance', DateType::class
            , ['widget' => 'choice',
            'years' => range(date('Y')-100, date('Y')-18)
            ])
            ->add('mail', TextType::class)
            ->add('portable', TextType::class)
            ->add('ville', TextType::class)
            ->add('sessions', EntityType::class, [
                'required' => false,
                'class' => 'App\Entity\Session',
                'choice_label' => 'nomSession',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Stagiaire::class,
        ]);
    }
}
