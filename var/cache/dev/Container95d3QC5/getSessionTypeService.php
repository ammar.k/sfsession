<?php

namespace Container95d3QC5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getSessionTypeService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Form\SessionType' shared autowired service.
     *
     * @return \App\Form\SessionType
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
        include_once \dirname(__DIR__, 4).'/src/Form/SessionType.php';

        return $container->privates['App\\Form\\SessionType'] = new \App\Form\SessionType();
    }
}
