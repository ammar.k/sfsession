<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* stagiaire/show.html.twig */
class __TwigTemplate_7915489c0e85a5fa7b0a5b104e1a0f90 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "stagiaire/show.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "stagiaire/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "stagiaire/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello StagiaireController!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1><i class=\"fa-solid fa-address-book\"></i> Fiche Stagiaire </h1>
        <h2> ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 7, $this->source); })()), "html", null, true);
        echo " </h2>
        <p>Sex : ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 8, $this->source); })()), "sex", [], "any", false, false, false, 8), "html", null, true);
        echo "</p>
        <p>Date de Naissance : ";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 9, $this->source); })()), "dateNaissance", [], "any", false, false, false, 9), "d/m/Y"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 9, $this->source); })()), "age", [], "any", false, false, false, 9), "html", null, true);
        echo " Ans</p>
        <p>Ville : ";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 10, $this->source); })()), "ville", [], "any", false, false, false, 10), "html", null, true);
        echo "</p>
        <p>Courrier : ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 11, $this->source); })()), "mail", [], "any", false, false, false, 11), "html", null, true);
        echo "</p>
        <p>Telephone : ";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 12, $this->source); })()), "portable", [], "any", false, false, false, 12), "html", null, true);
        echo "</p>
        <h2> Sessions Prevues </h2>
        ";
        // line 14
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 14, $this->source); })()), "sessions", [], "any", false, false, false, 14)) > 0)) {
            // line 15
            echo "        <ul>
            ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["stagiaire"]) || array_key_exists("stagiaire", $context) ? $context["stagiaire"] : (function () { throw new RuntimeError('Variable "stagiaire" does not exist.', 16, $this->source); })()), "sessions", [], "any", false, false, false, 16));
            foreach ($context['_seq'] as $context["_key"] => $context["session"]) {
                // line 17
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["session"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['session'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "        </ul>
        ";
        } else {
            // line 21
            echo "            <p> Aucune session prevue </p>
        ";
        }
        // line 23
        echo "

        
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "stagiaire/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 23,  140 => 21,  136 => 19,  127 => 17,  123 => 16,  120 => 15,  118 => 14,  113 => 12,  109 => 11,  105 => 10,  99 => 9,  95 => 8,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello StagiaireController!{% endblock %}

{% block body %}
    <h1><i class=\"fa-solid fa-address-book\"></i> Fiche Stagiaire </h1>
        <h2> {{ stagiaire}} </h2>
        <p>Sex : {{ stagiaire.sex }}</p>
        <p>Date de Naissance : {{ stagiaire.dateNaissance|date('d/m/Y') }}, {{stagiaire.age}} Ans</p>
        <p>Ville : {{ stagiaire.ville }}</p>
        <p>Courrier : {{ stagiaire.mail }}</p>
        <p>Telephone : {{ stagiaire.portable }}</p>
        <h2> Sessions Prevues </h2>
        {% if stagiaire.sessions | length > 0 %}
        <ul>
            {% for session in stagiaire.sessions %}
                <li>{{ session }}</li>
            {% endfor %}
        </ul>
        {% else %}
            <p> Aucune session prevue </p>
        {% endif %}


        
{% endblock %}", "stagiaire/show.html.twig", "/Applications/MAMP/htdocs/KOUZEHA_Ammar/session/templates/stagiaire/show.html.twig");
    }
}
