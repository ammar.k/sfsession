<?php

namespace ContainerSYnFTH2;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_LBhAYfEService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.LBhAYfE' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.LBhAYfE'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService ??= $container->getService(...), [
            'professeur' => ['privates', '.errored..service_locator.LBhAYfE.App\\Entity\\Professeur', NULL, 'Cannot autowire service ".service_locator.LBhAYfE": it needs an instance of "App\\Entity\\Professeur" but this type has been excluded in "config/services.yaml".'],
        ], [
            'professeur' => 'App\\Entity\\Professeur',
        ]);
    }
}
